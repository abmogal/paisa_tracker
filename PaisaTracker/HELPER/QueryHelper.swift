
import Foundation

class QueryHelper{
    static let QuerysharedInstance = QueryHelper()
    private init() {}
    
    func createTable(sqlQuery:String)->Bool{
        
        if db.openDB() {
            do {
                try db.dataBase.executeUpdate(sqlQuery, values: nil)
                db.closeDB()
                return true
            } catch {
                print(error)
            }
        }else{
            print("Database not open")
            
        }
        return false
    }
    
    func selectQuery(sqlQuery:String)->FMResultSet{
        
        var result : FMResultSet!
        
        if db.openDB() {
            result =  db.dataBase.executeQuery(sqlQuery, withArgumentsIn: [])
        }else{
            print("Database not open")
        }
        return result
    }
    
    func insertQuery(sqlQuery:String, valuesDict:[Any])->Bool{
        
        if db.openDB() {
            do {
                try db.dataBase.executeUpdate(sqlQuery  , values: valuesDict)
                
                return true
            } catch {
                print(error)
                return false
            }
        }else{
            print("Database not open")
        }
        return true
    }
}
