//
//  FCDBEngine.h


#import <Foundation/Foundation.h>
#import "FMDB.h"

@interface FCDBEngine : NSObject
@property (nonatomic, strong) FMDatabase *dataBase;
@property (nonatomic, strong) FMDatabaseQueue *dataBaseQueue;
+ (instancetype)engine;
- (BOOL)openDB;
- (BOOL)closeDB;
@end
