
//  DBQueryHelper.swift

import Foundation

class DBQueryHelper: NSObject{
    
    //Create Login Table
    func createLoginTable() {
        let tableName : String = "login"
        db.openDB()
        if db.openDB() {
            db.dataBaseQueue.inDatabase({(_ db: FMDatabase) -> Void in
                let createSQL: String = String.init(format: "CREATE TABLE IF NOT EXISTS %@ ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`user_id` INTEGER NOT NULL AUTOINCREMENT UNIQUE,`user_name` TEXT NOT NULL,`password` TEXT NOT NULL)",tableName as CVarArg)
                do {
                    try db.executeUpdate(createSQL, values: [])
//                    let rss = true
//                    if rss {
//                        if !db.columnExists("column_Name", inTableWithName: tableName) {
//                            do {
//                                try db.executeUpdate(String.init(format: "ALTER TABLE %@ ADD COLUMN column_Name TEXT DEFAULT ''",tableName), values: nil)
//                            } catch {
//                                print("error = \(error)")
//                            }
//                        }
//                    }
                }
                catch {
                    print("error = \(error)")
                }
            })
            db.closeDB()
        }
    }
    
   
    //Insert Into Login Table
    func insertOrUpdateLoginTable(_ detail: Any,isUpdate : Bool = false, result retHandler: @escaping (_ finished: Bool) -> Void) {
        let tableName : String = "login"
        var ret: Bool = false
        db.openDB()
        if db.openDB() {
            db.dataBaseQueue.inDatabase({(_ db: FMDatabase) -> Void in
                do {
                    // id,user_id,user_name,password
                    ret = true
                    if isUpdate {
                        let idValue : Int = Int("1")!
                        let rs = try db.executeQuery(String.init(format: "SELECT  * FROM %@ WHERE id = ?", tableName as CVarArg), values: [idValue])
                        if !(rs.isEqual(NSNull.self)) && (rs.next()) {
                            let updateSQL: String = String.init(format: "UPDATE %@ SET user_name = ? , password = ? WHERE id = ? ", tableName as CVarArg)
                            try db.executeUpdate(updateSQL, values: ["testac",
                                                                     "123456",
                                                                     idValue])
                            ret = true
                        }else{
                            let insertSQL: String = String.init(format: "INSERT INTO %@ (user_name,password)  VALUES (?,?)",tableName as CVarArg)
                            try db.executeUpdate(insertSQL, values: ["testac",
                                                                     "123456",
                                                                     ])
                            ret = true
                            
                        }
                        rs.close()
                    }else{
                        let insertSQL: String = String.init(format: "INSERT INTO %@ (user_name,password)  VALUES (?,?)",tableName as CVarArg)
                        try db.executeUpdate(insertSQL, values: ["testac",
                                                                 "123456",
                                                                 ])
                        ret = true
                    }
                } catch {
                    ret = false;
                    print("error = \(error)")
                }
            })
        }
        db.closeDB()
        retHandler(ret)
    }
    
    //Create incomeexpenses Table
    func createIncomeExpensesTable() {
        let tableName : String = "incomeexpenses"
        db.openDB()
        if db.openDB() {
            db.dataBaseQueue.inDatabase({(_ db: FMDatabase) -> Void in
                let createSQL: String = String.init(format: "CREATE TABLE IF NOT EXISTS %@ ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`amount` TEXT NOT NULL,`income_expense_type` TEXT NOT NULL,`date_ie` TEXT NOT NULL,`time_ie` TEXT NOT NULL,`description` TEXT,`amount_paid_type` TEXT NOT NULL,`income_or_expense` NUMERIC DEFAULT 0 NOT NULL)",tableName as CVarArg)
                do {
                    try db.executeUpdate(createSQL, values: [])
                    //                    let rss = true
                    //                    if rss {
                    //                        if !db.columnExists("column_Name", inTableWithName: tableName) {
                    //                            do {
                    //                                try db.executeUpdate(String.init(format: "ALTER TABLE %@ ADD COLUMN column_Name TEXT DEFAULT ''",tableName), values: nil)
                    //                            } catch {
                    //                                print("error = \(error)")
                    //                            }
                    //                        }
                    //                    }
                }
                catch {
                    print("error = \(error)")
                }
            })
            db.closeDB()
        }
    }
    
    //Insert Into Login Table
    func insertOrUpdateIncomeExpensesTable(_ dataStore: Incomeexpenses,isUpdate : Bool = false, result retHandler: @escaping (_ finished: Bool) -> Void) {
        let tableName : String = "incomeexpenses"
        var ret: Bool = false
        db.openDB()
        if db.openDB() {
            db.dataBaseQueue.inDatabase({(_ db: FMDatabase) -> Void in
                do {
                    // id,user_id,user_name,password
                    ret = true
                    if isUpdate {
                        let idValue : Int = Int("1")!
                        let rs = try db.executeQuery(String.init(format: "SELECT  * FROM %@ WHERE id = ?", tableName as CVarArg), values: [idValue])
                        if !(rs.isEqual(NSNull.self)) && (rs.next()) {
                            let updateSQL: String = String.init(format: "UPDATE %@ SET amount = ? , income_expense_type = ? , date_ie = ? , time_ie = ? , description = ? , amount_paid_type = ? , income_or_expense = ? WHERE id = ? ", tableName as CVarArg)
                            try db.executeUpdate(updateSQL, values: [dataStore.amount,
                                                                     dataStore.income_expense_type,
                                                                     dataStore.date_ie,
                                                                     dataStore.time_ie,
                                                                     dataStore.description,
                                                                     dataStore.amount_paid_type,
                                                                     dataStore.income_or_expense])
                            ret = true
                        }else{
                            let insertSQL: String = String.init(format: "INSERT INTO %@ (amount,income_expense_type,date_ie,time_ie,description,amount_paid_type,income_or_expense)  VALUES (?,?,?,?,?,?,?)",tableName as CVarArg)
                            try db.executeUpdate(insertSQL, values: [dataStore.amount,
                                                                     dataStore.income_expense_type,
                                                                     dataStore.date_ie,
                                                                     dataStore.time_ie,
                                                                     dataStore.description,
                                                                     dataStore.amount_paid_type,
                                                                     dataStore.income_or_expense])
                            ret = true
                            
                        }
                        rs.close()
                    }else{
                        let insertSQL: String = String.init(format: "INSERT INTO %@ (amount,income_expense_type,date_ie,time_ie,description,amount_paid_type,income_or_expense)  VALUES (?,?,?,?,?,?,?)",tableName as CVarArg)
                        try db.executeUpdate(insertSQL, values: [dataStore.amount,
                                                                 dataStore.income_expense_type,
                                                                 dataStore.date_ie,
                                                                 dataStore.time_ie,
                                                                 dataStore.description,
                                                                 dataStore.amount_paid_type,
                                                                 dataStore.income_or_expense])
                        ret = true
                    }
                } catch {
                    ret = false;
                    print("error = \(error)")
                }
            })
        }
        db.closeDB()
        retHandler(ret)
    }
    
    func SelectFromIncomeExpensesTable(result retHandler: @escaping (_ finished: [Incomeexpenses]) -> Void) {
        let tableName : String = "incomeexpenses"
        var ret = [Incomeexpenses]()
        db.openDB()
        if db.openDB() {
            db.dataBaseQueue.inDatabase({(_ db: FMDatabase) -> Void in
                do {
                    let rs = try db.executeQuery(String.init(format: "SELECT  * FROM %@ ", tableName as CVarArg), values: nil)
                    if !(rs.isEqual(NSNull.self)) {
                        while (rs as AnyObject).next() {
                            let tmp = Incomeexpenses()
                            
                            tmp.id = rs.object(forColumn: "id") as! Int
                            tmp.amount = rs.object(forColumn: "amount") as! String
                            tmp.income_expense_type = rs.object(forColumn: "income_expense_type") as! String
                           
                            tmp.date_ie = rs.object(forColumn: "date_ie") as! String
                            tmp.time_ie = rs.object(forColumn: "time_ie") as! String
                            tmp.description = rs.object(forColumn: "description") as! String
                            tmp.amount_paid_type = rs.object(forColumn: "amount_paid_type") as! String
                            tmp.income_or_expense = rs.object(forColumn: "income_or_expense") as! Int
                            
                            let doubleDate : Double = Double.init(tmp.date_ie) ?? 0
                            let tdate : Date = Date.init(timeIntervalSince1970: doubleDate)
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMYYYY"
                            var strDate : String = dateFormatter.string(from: tdate)
                            tmp.mmyyyy = NSNumber.init(value: Double.init(strDate) ?? 0.0).intValue
                            
                            let dateFormatterNew = DateFormatter()
                            dateFormatterNew.dateFormat = "dd"
                            strDate  = dateFormatterNew.string(from: tdate)
                            tmp.dd = NSNumber.init(value: Double.init(strDate) ?? 0.0).intValue
                            
                            ret.append(tmp)
                        }
                    }
                } catch {
                   // ret = false;
                    print("error = \(error)")
                }
            })
        }
        db.closeDB()
        retHandler(ret)
    }
}
