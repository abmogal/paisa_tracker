//
//  naviVC.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

class naviVC: UINavigationController {
    @IBOutlet var viewBar: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBar.frame = CGRect(x: 0, y: -CGFloat.init(self.navigationController?.navigationBar.frame.origin.y ?? CGFloat.init(0)), width: self.view.frame.size.width, height: (self.navigationController?.navigationBar.frame.size.height ?? 44) + CGFloat.init(self.navigationController?.navigationBar.frame.origin.y ?? CGFloat.init(0)))//(self.navigationController?.navigationBar.frame)!
        self.navigationBar.addSubview(viewBar)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.btnMenu.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.btnMenu.center.y = self.viewBar.center.y
        self.lblTitle.center.y = self.viewBar.center.y
        self.btnBack.center.y = self.viewBar.center.y
        
        self.viewBar.backgroundColor = backGroundColorCommon
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
