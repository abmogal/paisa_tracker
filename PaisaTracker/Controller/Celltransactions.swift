//
//  Celltransactions.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

protocol Altaf {
    func altafMogal()
}

class Celltransactions: UITableViewCell {
    
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblIncomeExpenseType: UILabel!
    
    @IBOutlet weak var btnDetail: UIButton!
    
    var delegate : Altaf!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //viewDay.layoutIfNeeded()
        viewDay.layer.masksToBounds = true
        viewDay.layer.cornerRadius = viewDay.frame.width/2
        
        btnDetail.layer.masksToBounds = true
        btnDetail.layer.cornerRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewDay.layer.masksToBounds = true
        viewDay.layer.cornerRadius = viewDay.frame.width/2
        
        btnDetail.layer.masksToBounds = true
        btnDetail.layer.cornerRadius = 10
        
        btnDetail.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    @IBAction func onClickbtnDeatil(_ sender: Any) {
        print("Button Click")
        
        self.delegate.altafMogal()
    }
}
