//
//  TransactionsVC.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

class TransactionsVC: BaseVC,UITableViewDelegate, UITableViewDataSource , Altaf{
    func altafMogal() {
        print("Altaf Protocol")
    }
    
    
    @IBOutlet weak var tblTransactions: UITableView!
    
    @IBOutlet weak var btnSort: UIButton!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var btnExcelSave: UIButton!
    
    @IBOutlet weak var btnPdfSave: UIButton!
    
    
    var allTransactions = [Incomeexpenses]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblTransactions.delegate = self
        tblTransactions.dataSource = self
        
        loadAllTranctions()
    }
    
    func loadAllTranctions(){
        DBQueryHendler.SelectFromIncomeExpensesTable{(response) in
            if response.count > 0 {
                self.allTransactions = response
            self.allTransactions = self.allTransactions.sorted(by: { $0.mmyyyy > $1.mmyyyy })
                
                print(response)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationVC.lblTitle.text = "All Income / Expenses "
        self.navigationVC.btnBack.isHidden = false
        self.navigationVC.btnMenu.addTarget(self, action: #selector(btnMenuNew), for: .touchUpInside)
        
        self.btnSort.backgroundColor = btnTiteleColorCommon
        self.btnFilter.backgroundColor = btnTiteleColorCommon
        self.btnExcelSave.backgroundColor = btnTiteleColorCommon
        self.btnPdfSave.backgroundColor = btnTiteleColorCommon
        
        self.btnSort.setTitleColor(backGroundColorCommon, for: .normal)
        self.btnFilter.setTitleColor(backGroundColorCommon, for: .normal)
        self.btnExcelSave.setTitleColor(backGroundColorCommon, for: .normal)
        self.btnPdfSave.setTitleColor(backGroundColorCommon, for: .normal)
        
        self.btnSort.layer.borderWidth = 0.5
        self.btnSort.layer.borderColor = backGroundColorCommon.cgColor
        
        self.btnFilter.layer.borderWidth = 0.5
        self.btnFilter.layer.borderColor = backGroundColorCommon.cgColor
        
        self.btnExcelSave.layer.borderWidth = 0.5
        self.btnExcelSave.layer.borderColor = backGroundColorCommon.cgColor
        
        self.btnPdfSave.layer.borderWidth = 0.5
        self.btnPdfSave.layer.borderColor = backGroundColorCommon.cgColor
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationVC.btnMenu.removeTarget(self, action: #selector(btnMenuNew), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transCell", for: indexPath) as! Celltransactions
        
//        if indexPath.row % 2 == 0{
//            cell.viewDay.backgroundColor = UIColor.orange
//        }else{
//            cell.viewDay.backgroundColor = UIColor.red
//        }
        cell.delegate = self
        cell.lblDay.text = String.init(allTransactions[indexPath.row].dd)
        cell.lblAmount.text = allTransactions[indexPath.row].amount
        cell.lblIncomeExpenseType.text = allTransactions[indexPath.row].income_expense_type
       
        return cell
    }
    
    @objc func btnMenuNew() {
        print(" Trans Menu")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionHeaderTitle = "Section - \(section)"
        
      return sectionHeaderTitle
    }
    
    @IBAction func onClickBtnSort(_ sender: Any) {
        print("Sort")
    }
    
    
    @IBAction func onClickBtnFilter(_ sender: Any) {
        print("Filter")
    }
    
    
    @IBAction func onClickBtnExcelSave(_ sender: Any) {
        print("Excel")
    }
    
    @IBAction func onClickBtnPdfSave(_ sender: Any) {
        print("Pdf")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
