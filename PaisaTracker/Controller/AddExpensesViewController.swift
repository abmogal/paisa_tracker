//
//  AddExpensesViewController.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

class AddExpensesViewController: BaseVC , UIPickerViewDelegate,UIPickerViewDataSource ,UITextFieldDelegate , UITextViewDelegate{
    
    @IBOutlet weak var btnSaveExpenses: UIButton!
    
    @IBOutlet weak var scrViewExpenses: UIScrollView!
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnTypeOfIncome: UIButton!
    
    @IBOutlet weak var btnAmountPaidOption: UIButton!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var btnDate: UIButton!
    
    @IBOutlet weak var btnTime: UIButton!
    
    @IBOutlet weak var viewContainer: UIView!
    
    var isTypeOfIncome = true
    
    var pickerViewCommon : UIPickerView!
    
    var datePickerCommon : UIDatePicker!
    
    var typeOfIncomeArray = [String]()
    
    var amoutPaidOptionArray = [String]()
    
    var ViewForPickerView = UIView()
    
    var incomeexpenseAdd = Incomeexpenses()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerViewCommon = UIPickerView()
        pickerViewCommon.delegate = self
        pickerViewCommon.dataSource = self
        
        typeOfIncomeArray = ["Business","Loan","Salary"]
        amoutPaidOptionArray = ["Cash","Credit Card","Debit Card","Net Banking"]
        
        self.txtAmount.layer.borderWidth = 0.5
        self.txtAmount.layer.cornerRadius = 5
        
        self.btnTypeOfIncome.layer.borderWidth = 0.5
        self.btnTypeOfIncome.layer.cornerRadius = 5
        
        self.btnDate.layer.borderWidth = 0.5
        self.btnDate.layer.cornerRadius = 5
        
        self.btnTime.layer.borderWidth = 0.5
        self.btnTime.layer.cornerRadius = 5
        
        self.txtViewDescription.layer.borderWidth = 0.5
        self.txtViewDescription.layer.cornerRadius = 5
        
        self.btnAmountPaidOption.layer.borderWidth = 0.5
        self.btnAmountPaidOption.layer.cornerRadius = 5
        
//        self.btnSaveExpenses.layer.borderWidth = 0.5
//        self.btnSaveExpenses.layer.cornerRadius = 5
        
        self.pickerViewCommon.layer.borderWidth =  0.5
        self.view.addSubview(pickerViewCommon)
        self.pickerViewCommon.isHidden = true
        
        datePickerCommon = UIDatePicker()
        
        datePickerCommon.addTarget(self, action: #selector(valueChangedDatePicker), for: .valueChanged)
        self.datePickerCommon.isHidden  = true
        
        datePickerCommon.center = self.view.center
        datePickerCommon.backgroundColor = UIColor.lightGray
        
        self.view.addSubview(ViewForPickerView)
        
        ViewForPickerView.frame = self.view.frame
        ViewForPickerView.isUserInteractionEnabled = true
        
        ViewForPickerView.isHidden = true
        ViewForPickerView.backgroundColor =  UIColor(white: 1, alpha: 0.5)
        
        ViewForPickerView.addSubview(pickerViewCommon)
        ViewForPickerView.addSubview(datePickerCommon)
        
        txtAmount.becomeFirstResponder()
        
        self.btnDate.setTitle(self.toStringFromDateTime(dateTimeGiven: Date(), mode: "date"), for: .normal)
        
        self.btnTime.setTitle(self.toStringFromDateTime(dateTimeGiven: Date(), mode: "time"), for: .normal)
        
        txtAmount.delegate =  self
        
        txtViewDescription.delegate = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isTypeOfIncome ==  true {
            return typeOfIncomeArray.count
        }else{
            return amoutPaidOptionArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isTypeOfIncome ==  true {
            return typeOfIncomeArray[row]
        }else{
            return amoutPaidOptionArray[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ViewForPickerView.isHidden = true
        if isTypeOfIncome ==  true {
            btnTypeOfIncome.setTitle(typeOfIncomeArray[row], for: .normal)
        }else{
            btnAmountPaidOption.setTitle(amoutPaidOptionArray[row], for: .normal)
        }
        pickerViewCommon.isHidden = true
    }
    
    @objc func btnMenuNew(){
        print("menu new 2")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationVC.lblTitle.text = "Add Expenses "
        self.navigationVC.btnBack.isHidden = false
        self.navigationVC.btnMenu.addTarget(self, action: #selector(btnMenuNew), for: .touchUpInside)
        
        self.btnSaveExpenses.backgroundColor = backGroundColorCommon
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       self.navigationVC.btnMenu.removeTarget(self, action: #selector(btnMenuNew), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        self.btnTypeOfIncome.imageEdgeInsets  = UIEdgeInsets(top:0 , left: self.btnTypeOfIncome.frame.width - 25, bottom: 0, right: 0)
        
        self.btnAmountPaidOption.imageEdgeInsets  = UIEdgeInsets(top:0 , left: self.btnAmountPaidOption.frame.width - 25, bottom: 0, right: 0)
    }
    
    
    @IBAction func btnTypeOfIncomeClick(_ sender: Any) {
        ViewForPickerView.isHidden = false
        isTypeOfIncome = true
        pickerViewCommon.isHidden = false
        pickerViewCommon.contentMode = .center
        pickerViewCommon.backgroundColor = UIColor.gray
        pickerViewCommon.frame = CGRect.init(x: self.btnTypeOfIncome.frame.origin.x, y: self.btnTypeOfIncome.frame.origin.y+(self.btnTypeOfIncome.frame.height/2)+100, width: self.btnTypeOfIncome.frame.width , height: 130)
        pickerViewCommon.reloadAllComponents()
    }
    
    @IBAction func btnOnClickAmountPaid(_ sender: Any) {
        ViewForPickerView.isHidden = false
        isTypeOfIncome = false
        pickerViewCommon.isHidden = false
        pickerViewCommon.contentMode = .center
        pickerViewCommon.backgroundColor = UIColor.gray
        pickerViewCommon.frame = CGRect.init(x: self.btnAmountPaidOption.frame.origin.x, y: self.btnAmountPaidOption.frame.origin.y+(self.btnAmountPaidOption.frame.height/2)+100, width: self.btnAmountPaidOption.frame.width , height: 100)
        pickerViewCommon.reloadAllComponents()
    }
    
    @IBAction func btnOnClickDate(_ sender: Any) {
        //  ViewForPickerView.frame = self.view.frame
        ViewForPickerView.isHidden = false
        datePickerCommon.datePickerMode = .date
        datePickerCommon.backgroundColor = UIColor.gray
        datePickerCommon.isHidden = false
        
        print("Date")
    }
    
    @IBAction func btnOnClickTime(_ sender: Any) {
        // ViewForPickerView.frame = self.view.frame
        ViewForPickerView.isHidden = false
        datePickerCommon.datePickerMode  = .time
        datePickerCommon.backgroundColor = UIColor.gray
        datePickerCommon.isHidden = false
        
        print("Time")
    }
    
    @IBAction func btnOnClickSave(_ sender: Any) {
        
        if self.txtAmount.text == "" || self.txtAmount.text == nil {
            APP_DELEGATE.showalertwithself(_self: self, message: "Please enter Amount", title: "Alert : Blank not allow!!!", withCompletion: { response in
                self.txtAmount.becomeFirstResponder()
                return
            })
            let num = Int(txtAmount.text!)
            if num == nil {
                APP_DELEGATE.showalertwithself(_self: self, message: "Please enter valid Amount", title: "Alert : Amount not allow!!!", withCompletion: { response in
                    self.txtAmount.becomeFirstResponder()
                    return
                })
            }
        }else if btnTypeOfIncome.currentTitle == "" || self.btnTypeOfIncome.currentTitle == nil {
            APP_DELEGATE.showalertwithself(_self: self, message: "Please enter type of Income", title: "Alert : Blank not allow!!!", withCompletion: { response in
                self.btnTypeOfIncome.becomeFirstResponder()
                return
            })
            
        }else if btnDate.currentTitle == "" || self.btnDate.currentTitle == nil {
            APP_DELEGATE.showalertwithself(_self: self, message: "Please enter Date", title: "Alert : Blank not allow!!!", withCompletion: { response in
                self.btnDate.becomeFirstResponder()
                return
            })
        }else if btnTime.currentTitle == "" || self.btnTime.currentTitle == nil {
            APP_DELEGATE.showalertwithself(_self: self, message: "Please enter Time", title: "Alert : Blank not allow!!!", withCompletion: { response in
                self.btnTime.becomeFirstResponder()
                return
            })
        }else if btnAmountPaidOption.currentTitle == "" || self.btnAmountPaidOption.currentTitle == nil {
            APP_DELEGATE.showalertwithself(_self: self, message: "Please enter Amount Type", title: "Alert : Blank not allow...", withCompletion: { response in                self.btnAmountPaidOption.becomeFirstResponder()
                return
            })
        }
        
        incomeexpenseAdd.amount = txtAmount.text!
        incomeexpenseAdd.income_expense_type = btnTypeOfIncome.currentTitle!
        
        //Date //Time add when select date and time
       
        if txtViewDescription.text == "" || txtViewDescription.text == "Description (Optional)" {
            incomeexpenseAdd.description = ""
        }
        else{
            incomeexpenseAdd.description = txtViewDescription.text
        }
        incomeexpenseAdd.income_or_expense = 0
        
        DBQueryHendler.insertOrUpdateIncomeExpensesTable(incomeexpenseAdd) { (response) in
            if response {
                APP_DELEGATE.showalertwithself(_self: self, message: "Data save sucessfully", title: "Alert : Sucess.", withCompletion: { response in
                    self.txtAmount.text = ""
                    self.txtViewDescription.text = "Description (Optional)"
                    self.btnTime.setTitle(self.toStringFromDateTime(dateTimeGiven: Date(), mode: "time"), for: .normal)
                    self.txtAmount.becomeFirstResponder()
                    return
                })
            }else {
                
            }
        }
        
    }
    
    @objc func valueChangedDatePicker(){
        if datePickerCommon.datePickerMode == .date {
            self.btnDate.setTitle(self.toStringFromDateTime(dateTimeGiven: datePickerCommon.date, mode: "date"), for: .normal)
        }else{
            self.btnTime.setTitle(self.toStringFromDateTime(dateTimeGiven: datePickerCommon.date, mode: "time"), for: .normal)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first?.view == self.view || touches.first?.view == self.viewContainer || touches.first?.view == scrViewExpenses || touches.first?.view == viewContainer || touches.first?.view == self.ViewForPickerView{
            
            if  touches.first?.view == self.ViewForPickerView {
                ViewForPickerView.isHidden = true
                // ViewForPickerView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            }
            if self.pickerViewCommon.isHidden == false {
                self.pickerViewCommon.isHidden = true
            }
            if self.datePickerCommon.isHidden == false {
                self.datePickerCommon.isHidden = true
            }
        }
    }
    
    @objc func donePicker(){
        ViewForPickerView.isHidden = true
    }
    
    func toStringFromDateTime(dateTimeGiven : Date , mode : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        if mode == "date" {
            //Store
            incomeexpenseAdd.date_ie = String.init(format: "%.0f",dateTimeGiven.timeIntervalSince1970 as CVarArg)
            print(dateTimeGiven.timeIntervalSince1970)
            //Received
            let receivedDate = Date.init(timeIntervalSince1970: dateTimeGiven.timeIntervalSince1970)
            print(dateFormatter.string(from: receivedDate))
            //
            return dateFormatter.string(from: dateTimeGiven)
            
        }else{
            dateFormatter.dateFormat = "hh:mm a"
            
            //Store
            incomeexpenseAdd.time_ie = String.init(format: "%.0f",dateTimeGiven.timeIntervalSince1970 as CVarArg)
            print(dateTimeGiven.timeIntervalSince1970)
            //Received
            let receivedDate = Date.init(timeIntervalSince1970: dateTimeGiven.timeIntervalSince1970)
            print(dateFormatter.string(from: receivedDate))
            
            
            let selectedtime: String = dateFormatter.string(from: datePickerCommon.date)
            return selectedtime
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtAmount {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            
            if filtered == string {
                return true
            } else {
                if string == "." {
                    let countdots = textField.text!.components(separatedBy:".").count - 1
                    if countdots == 0 {
                        return true
                    }else{
                        if countdots > 0 && string == "." {
                            return false
                        } else {
                            return true
                        }
                    }
                }else{
                    return false
                }
            }
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView == txtViewDescription {
            if textView.text == "Description (Optional)" {
                textView.text = ""
            }else if textView.text == "" || textView.text == nil {
                textView.text = "Description (Optional)"
            }
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtViewDescription {
            if textView.text == "" || textView.text == nil {
                textView.text = "Description (Optional)"
            }
        }
    }
}
