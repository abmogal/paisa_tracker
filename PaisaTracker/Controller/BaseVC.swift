//
//  BaseVC.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
//@IBOutlet var viewBar: UIView!
//    
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnMenu: UIButton!
    
    var navigationVC = naviVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if self.navigationController is naviVC
        {
            self.navigationVC = self.navigationController as! naviVC
        }
        self.navigationVC.lblTitle.text = "Base VC"
        
       self.navigationVC.btnBack.addTarget(self, action: #selector(btnClickNew), for: .touchUpInside)
//
//        self.navigationVC.btnMenu.addTarget(self, action: #selector(btnMenuNew), for: .touchUpInside)
    }
    
    @objc func btnClickNew(){
    self.navigationController?.popViewController(animated: true)
        
    }
//    @objc func btnMenuNew(){
//        print("base menu new ")
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
