//
//  AppDelegate.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import UIKit

let APP_DELEGATE = AppDelegate.shareApplication()
let db = FCDBEngine.init()
let DBQueryHendler = DBQueryHelper()

let backGroundColorCommon = UIColor.init(red: 255/255, green: 132/255, blue: 21/255, alpha: 1.0)

let btnTiteleColorCommon = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)

typealias CompletionHandler = (_ success:Bool) -> Void

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    class func shareApplication() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DBQueryHendler.createIncomeExpensesTable()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func showalertwithself(_self:UIViewController , message:String , title:String , withCompletion:@escaping CompletionHandler)
    {
        let alertController = UIAlertController(title:title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default) { action in
            alertController.dismiss(animated: true, completion: nil)
            withCompletion(true)
        }
        
        alertController.addAction(cancelAction)
        _self.present(alertController, animated: true)
    }
}
