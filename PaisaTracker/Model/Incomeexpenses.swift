//
//  Incomeexpenses.swift
//  PaisaTracker
//
//  Created by Altaf Mogal.
//  Copyright © 2019 aaa. All rights reserved.
//

import Foundation

class Incomeexpenses{
    
    var id : Int
    var amount : String
    var income_expense_type  : String
    var date_ie : String
    var time_ie : String
    var description : String
    var amount_paid_type : String
    var income_or_expense : Int
    var mmyyyy : Int
    var dd : Int
    
    
    init(id: Int, amount : String, income_expense_type  : String , date_ie : String , time_ie : String , description : String , amount_paid_type : String , income_or_expense : Int ,mmyyyy : Int, dd : Int) {
        self.id = id
        self.amount = amount
        self.income_expense_type = income_expense_type
        self.date_ie = date_ie
        self.time_ie = time_ie
        self.description = description
        self.amount_paid_type = amount_paid_type
        self.income_or_expense = income_or_expense
        self.mmyyyy = mmyyyy
        self.dd = dd
    }
    
    init() {
        self.id = 0
        self.amount = ""
        self.income_expense_type = ""
        self.date_ie = ""
        self.time_ie = ""
        self.description = ""
        self.amount_paid_type = ""
        self.income_or_expense = 0
        self.mmyyyy = 0
        self.dd = 0
    }
   
}
